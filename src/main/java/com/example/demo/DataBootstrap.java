package com.example.demo;

import com.example.demo.dao.DowJonesIndexRepository;
import com.example.demo.data.DowJonesIndex;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataBootstrap {
    private static final String DATASTORE_LOCATION = "/static/dow_jones_index.csv";

    @Autowired
    private DowJonesIndexRepository dowJonesIndexRepository;

    @PostConstruct
    public void init() {
    	try {
	        InputStream inputStream = this.getClass().getResourceAsStream(DATASTORE_LOCATION);
	        CSVParser parser = CSVFormat.DEFAULT.withHeader().parse(new InputStreamReader(inputStream, StandardCharsets.UTF_8.name()));
	        List<DowJonesIndex> dowJonesIndex = new ArrayList<>();
	        try {
	        	List<CSVRecord> records = parser.getRecords();
	        	records.forEach(record -> {
	        		DowJonesIndex indx = new DowJonesIndex();
	        		if(record.get("close") != null && record.get("close") != "")
	        			indx.setClose(Float.parseFloat(getRate(record.get("close"))));
	        		if(record.get("date") != null && record.get("date") != "")
	        			indx.setDate(record.get("date"));
	        		if(record.get("days_to_next_dividend") != null && record.get("days_to_next_dividend") != "")
	        			indx.setDays_to_next_dividend(Integer.parseInt(record.get("days_to_next_dividend")));
	        		if(record.get("high") != null && record.get("high") != "")
	        			indx.setHigh(Float.parseFloat(getRate(record.get("high"))));
	        		if(record.get("low") != null && record.get("low") != "")
	        			indx.setLow(Float.parseFloat(getRate(record.get("low"))));
	        		if(record.get("next_weeks_close") != null && record.get("next_weeks_close") != "")
	        			indx.setNext_weeks_close(Float.parseFloat(getRate(record.get("next_weeks_close"))));
	        		if(record.get("next_weeks_open") != null && record.get("next_weeks_open") != "")
	        			indx.setNext_weeks_open(Float.parseFloat(getRate(record.get("next_weeks_open"))));
	        		if(record.get("open") != null && record.get("open") != "")
	        			indx.setOpen(Float.parseFloat(getRate(record.get("open"))));
	        		if(record.get("percent_change_volume_over_last_wk") != null && record.get("percent_change_volume_over_last_wk") != "")
	        			indx.setPercent_change_volume_over_last_wk(Float.parseFloat(getRate(record.get("percent_change_volume_over_last_wk"))));
	        		if(record.get("percent_change_next_weeks_price") != null && record.get("percent_change_next_weeks_price") != "")
	        			indx.setPercent_change_next_weeks_price(Float.parseFloat(getRate(record.get("percent_change_next_weeks_price"))));
	        		if(record.get("percent_change_price") != null && record.get("percent_change_price") != "")
	        			indx.setPercent_change_price(Float.parseFloat(getRate(record.get("percent_change_price"))));
	        		if(record.get("percent_return_next_dividend") != null && record.get("percent_return_next_dividend") != "")
	        			indx.setPercent_return_next_dividend(Float.parseFloat(getRate(record.get("percent_return_next_dividend"))));
	        		if(record.get("previous_weeks_volume") != null && record.get("previous_weeks_volume") != "")
	        			indx.setPrevious_weeks_volume(Float.parseFloat(getRate(record.get("previous_weeks_volume"))));
	        		if(record.get("quarter") != null && record.get("quarter") != "")
	        			indx.setQuarter(Integer.parseInt(record.get("quarter")));
	        		if(record.get("stock") != null && record.get("stock") != "")
	        			indx.setStock(record.get("stock"));
	        		if(record.get("volume") != null && record.get("volume") != "")
	        			indx.setVolume(Integer.parseInt(record.get("volume")));
	        		dowJonesIndex.add(indx);
	        	});
	        } catch (IOException e) {
	            throw new RuntimeException("Error in populating the record" +e);
	        }
	        //insert into repository.......
	    	dowJonesIndexRepository.saveAll(dowJonesIndex);
    	}  catch (IOException e) {
            throw new RuntimeException("Error while trying to read the csv file" + e);
        }
    }

	private String getRate(String rate) {
		if(rate.contains("$")) {
			int i = rate.indexOf("$");
			return rate.substring(i+1);
		}
		return rate;
	}
}
