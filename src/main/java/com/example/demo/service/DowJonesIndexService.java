package com.example.demo.service;

import java.util.List;

import com.example.demo.data.DowJonesIndex;

public interface DowJonesIndexService {
	DowJonesIndex create(DowJonesIndex indx);
	List<DowJonesIndex> read(String stock);
	boolean bulkUpload(List<DowJonesIndex> indexes);
}
