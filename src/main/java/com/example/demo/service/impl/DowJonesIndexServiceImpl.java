package com.example.demo.service.impl;

import com.example.demo.dao.DowJonesIndexRepository;
import com.example.demo.data.DowJonesIndex;
import com.example.demo.service.DowJonesIndexService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.transaction.Transactional;

@Service
public class DowJonesIndexServiceImpl implements DowJonesIndexService {

    private static final Logger LOG = LoggerFactory.getLogger(DowJonesIndexServiceImpl.class);

    @Autowired
    private DowJonesIndexRepository dowJonesIndexRepository;

	@Override
	@Transactional
	public DowJonesIndex create(DowJonesIndex indx) {
		// TODO Auto-generated method stub
		LOG.debug("Received indx create request for [{}]", indx);
		dowJonesIndexRepository.save(indx);
		return indx;
	}

	@Override
	public List<DowJonesIndex> read(String stock) {
		// TODO Auto-generated method stub
		LOG.debug("Received stock read request for stock [{}]", stock);
		return dowJonesIndexRepository.findByStock(stock);
	}

	@Override
	@Transactional
	public boolean bulkUpload(List<DowJonesIndex> indexes) {
		// TODO Auto-generated method stub
		List<DowJonesIndex> savedEntities = dowJonesIndexRepository.saveAll(indexes);
		if(savedEntities.size() == indexes.size())
			return true;
		return false;
	}

    
}
