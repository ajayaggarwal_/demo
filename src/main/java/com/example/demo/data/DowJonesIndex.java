package com.example.demo.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="DowJonesIndex")
public class DowJonesIndex {

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name="quarter")
	private int quarter;
	
	@Column(name="stock")
	private String stock;
	
	@Column(name="date")
	private String date;
	
	@Column(name="open")
	private float open;
	
	@Column(name="high")
	private float high;
	
	@Column(name="low")
	private float low;
	
	@Column(name="close")
	private float close;
	
	@Column(name="volume")
	private int volume;
	
	@Column(name="percent_change_price")
	private float percent_change_price;
	
	@Column(name="percent_change_volume_over_last_wk")
	private float percent_change_volume_over_last_wk;
	
	@Column(name="previous_weeks_volume")
	private float previous_weeks_volume;
	
	@Column(name="next_weeks_open")
	private float next_weeks_open;
	
	@Column(name="next_weeks_close")
	private float next_weeks_close;
	
	@Column(name="percent_change_next_weeks_price")
	private float percent_change_next_weeks_price;
	
	@Column(name="days_to_next_dividend")
	private int days_to_next_dividend;
	
	@Column(name="percent_return_next_dividend")
	private float percent_return_next_dividend;
	
	public int getQuarter() {
		return quarter;
	}
	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public float getOpen() {
		return open;
	}
	public void setOpen(float open) {
		this.open = open;
	}
	public float getHigh() {
		return high;
	}
	public void setHigh(float high) {
		this.high = high;
	}
	public float getLow() {
		return low;
	}
	public void setLow(float low) {
		this.low = low;
	}
	public float getClose() {
		return close;
	}
	public void setClose(float close) {
		this.close = close;
	}
	public int getVolume() {
		return volume;
	}
	public void setVolume(int volume) {
		this.volume = volume;
	}
	public float getPercent_change_price() {
		return percent_change_price;
	}
	public void setPercent_change_price(float percent_change_price) {
		this.percent_change_price = percent_change_price;
	}
	public float getPercent_change_volume_over_last_wk() {
		return percent_change_volume_over_last_wk;
	}
	public void setPercent_change_volume_over_last_wk(float percent_change_volume_over_last_wk) {
		this.percent_change_volume_over_last_wk = percent_change_volume_over_last_wk;
	}
	public float getPrevious_weeks_volume() {
		return previous_weeks_volume;
	}
	public void setPrevious_weeks_volume(float previous_weeks_volume) {
		this.previous_weeks_volume = previous_weeks_volume;
	}
	public float getNext_weeks_open() {
		return next_weeks_open;
	}
	public void setNext_weeks_open(float next_weeks_open) {
		this.next_weeks_open = next_weeks_open;
	}
	public float getNext_weeks_close() {
		return next_weeks_close;
	}
	public void setNext_weeks_close(float next_weeks_close) {
		this.next_weeks_close = next_weeks_close;
	}
	public float getPercent_change_next_weeks_price() {
		return percent_change_next_weeks_price;
	}
	public void setPercent_change_next_weeks_price(float percent_change_next_weeks_price) {
		this.percent_change_next_weeks_price = percent_change_next_weeks_price;
	}
	public int getDays_to_next_dividend() {
		return days_to_next_dividend;
	}
	public void setDays_to_next_dividend(int days_to_next_dividend) {
		this.days_to_next_dividend = days_to_next_dividend;
	}
	public float getPercent_return_next_dividend() {
		return percent_return_next_dividend;
	}
	public void setPercent_return_next_dividend(float percent_return_next_dividend) {
		this.percent_return_next_dividend = percent_return_next_dividend;
	}
	
}
