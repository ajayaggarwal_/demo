package com.example.demo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.data.DowJonesIndex;

@Repository
public interface DowJonesIndexRepository extends JpaRepository<DowJonesIndex, Integer> {
	List<DowJonesIndex> findByStock(String stock);
}
