package com.example.demo.controller;

import com.example.demo.data.DowJonesIndex;
import com.example.demo.service.DowJonesIndexService;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class DowJonesIndexController {
    private static final Logger LOG = LoggerFactory.getLogger(DowJonesIndexController.class);

    @Autowired
    private DowJonesIndexService dowJonesIndexService;
    
    @PostMapping("/createIndex")
    public DowJonesIndex create(@RequestBody DowJonesIndex index) {
        LOG.debug("Received index create request for [{}]", index);
        return dowJonesIndexService.create(index);
    }

    @GetMapping("/readIndex/{stock}")
    public List<DowJonesIndex> read(@PathVariable String stock) {
        LOG.debug("Received stock read request for stock [{}]", stock);
        return dowJonesIndexService.read(stock);
    }
    
    @PostMapping("/bulkUploadIndexes")
    public boolean bulkUpload(@RequestBody List<DowJonesIndex> indexes) {
        LOG.debug("Received index bulk upload request of size [{}]", indexes.size());
        return dowJonesIndexService.bulkUpload(indexes);
    }

}
